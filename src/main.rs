use rayon::prelude::*;
use indicatif::{ProgressBar, ProgressStyle, ParallelProgressIterator, ProgressIterator};
use std::iter::{self, Iterator};
use std::cmp::Ord;
use std::convert::identity;
use image::GrayImage;

const N: u64 = 1000;
const WITH_POWERS: bool = false;


fn merge_sorted<'a, T, I>(iters: &'a mut [I]) -> impl Iterator<Item = T> + 'a
        where T: Ord + Copy + 'a, I: Iterator<Item = T> {

    let mut peeks: Vec<Option<T>> = iters.iter_mut().map(|i| i.next()).collect();

    iter::from_fn(move || {
        if let Some((i, p)) = iters.iter_mut()
                .zip(peeks.iter_mut())
                .filter(|(_i, p)| p.is_some())
                .min_by_key(|(_i, p)| p.unwrap()) {
            let ret = *p;
            *p = i.next();
            Some(ret)
        }
        else {
            None
        }
    })
    .filter_map(identity)
}


fn main() {
    let prog_style = ProgressStyle::default_bar()
            .progress_chars("=>.")
            .template("{percent:>3.0}% [{bar:60}] eta: {eta_precise}");

    eprint!("generating primes...");
    let mut primes = generate_primes(N);
    eprintln!(" done");

    if WITH_POWERS {
        eprint!("adding prime powers...");
        let mut prime_pow_iters = primes.into_iter()
            .map(|p| iter::successors(Some(p), move |q| q.checked_mul(p)).take_while(|q| *q < N))
            .collect::<Vec<_>>();
        primes = merge_sorted(&mut prime_pow_iters).collect();
        eprintln!(" done");
    }

    eprintln!("generating table...");
    let results = (1..N).into_par_iter()
        .progress_with(ProgressBar::new(N as u64).with_style(prog_style.clone()))
        .map(|i| primes.iter().rev()
                        .scan(i, |i, p| {
                            let mut count = 0_u8;

                            while *i % p == 0 && *i > 1 {
                                *i /= p;
                                count += 1;
                            }

                            Some(count)
                        })
                        .collect::<Vec<_>>())
        .collect::<Vec<_>>();


    eprintln!("creating image...");
    let max = *results.iter().filter_map(|v| v.iter().max()).max().unwrap();
    let buf: Vec<_> = results.into_iter()
        .progress_with(ProgressBar::new(N as u64).with_style(prog_style.clone()))
        .flat_map(|v| v.into_iter())
        .map(|x| ((x as f32 / max as f32) * 255.0) as u8).collect();
    let img = GrayImage::from_vec(primes.len() as u32, N as u32 - 1, buf).unwrap();
    img.save("img.png").unwrap();

    //eprintln!("writing result...");
    //results.into_iter()
    //    .progress_with(ProgressBar::new(N as u64).with_style(prog_style.clone()))
    //    .for_each(|es| println!("{}", es.into_iter().fold(String::new(), |s, e| s + ", " + &e.to_string())));
}

fn generate_primes(up_to: u64) -> Vec<u64> {
    let mut primes = Vec::with_capacity(max_number_of_primes_below(up_to) as usize);
    primes.push(2);

    for candidate in (3..up_to).step_by(2) {
        if primes.iter()
                .take_while(|&&p| p * p <= candidate)
                .any(|p| candidate % p == 0) {
            continue;
        }
        else {
            primes.push(candidate);
        }
    }

    primes
}

fn max_number_of_primes_below(n: u64) -> u64 {
    if n < 3 {
        1
    }
    else {
        let n = n as f64;
        //(n / (n.ln() - 4.0)) u64  // from https://math.stackexchange.com/a/54325/436837
        (n / n.ln() * (1.0 + 1.3 / n.ln())) as u64  // from https://math.stackexchange.com/a/54318/436837
    }
}
